/*
This program serves a web page with a search bar allowing users
to search documents. Documents may be either public or private.
Using SQL injection, it will be possible to query private docs.

Cookies will also be vulnerable to SQL injection.
*/

package main

import(
	"fmt"
	"net/http"
	"html/template"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"crypto/md5"
	"time"
)

func main() {
	http.HandleFunc("/search", search)
	http.HandleFunc("/login", login)
	http.HandleFunc("/logout", logout)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

type Result struct {
	Title string
	Privacy string
}

type Results struct {
	Entries []Result
}

/*
A cookie will be used to determine whether a user can view
private documents. A successful SQL injection will allow
a user to query private documents without this cookie set.
SQL injection can also be used with the cookie itself to
bypass User.permissions.

A malicious user can log in with a public-permissions-only
account and modify their session_id to include "'--" at the
end to bypass the privilege check.
*/
func search(w http.ResponseWriter, r *http.Request) {
	searchHTML, _ := template.ParseFiles("front_end_search.html")
	if r.Method == "GET" {
		// Serve search bar HTML
		searchHTML.Execute(w, nil)
	} else {
		// Consume cookies, search terms
		r.ParseForm()
		searchTerms := r.Form["search"][0]
		var user string
		var docsQuery string
		sessCookie, cookerr := r.Cookie("session_id")
		if cookerr != nil {
			// No user logged in, search public files
			log.Println("/search: session_id cookie not found")
			docsQuery = "SELECT title, privacy FROM Documents WHERE title LIKE '%" + 
					searchTerms + "%' AND privacy = 'public';"
		} else {
			// User logged in
			// Figure out whether they have private permissions
			sessionQuery := `SELECT User.username FROM Session, User WHERE 
				 Session.username = User.username
				 AND Session.session_id = '` + sessCookie.Value + 
				 `' AND User.permissions = 'private';`
			log.Println("/search session query: " + sessionQuery)
			
			Database, err := sql.Open("sqlite3", "keepmesafe.db")
			if err != nil {
				log.Fatal(err)
			}
			defer Database.Close()
			err = Database.QueryRow(sessionQuery).Scan(&user)
			if err != nil {
				// User is public-only
				log.Println("/search: public-only user: " + user)
				log.Println("/search login check:", fmt.Sprint(err))
				docsQuery = "SELECT title, privacy FROM Documents WHERE title LIKE '%" + 
					searchTerms + "%' AND privacy = 'public';"
			} else {
				// User has private viewing privileges
				log.Println("/search: private-accessable user: " + user)
				docsQuery = "SELECT title, privacy FROM Documents WHERE title LIKE '%" + searchTerms + "%';"
			}
		}
		
		var rows *sql.Rows
		log.Println("/search docsQuery: " + docsQuery)
		Database, err := sql.Open("sqlite3", "keepmesafe.db")
		if err != nil {
			log.Fatal(err)
		}
		defer Database.Close()
		rows, queryerr := Database.Query(docsQuery)
		if queryerr != nil {
			fmt.Fprintf(w, fmt.Sprint(queryerr))
			log.Println(queryerr)
			return
		} else {
			defer rows.Close()
			results := Results{}
			for rows.Next() {
				var title string
				var privacy string
				scanerr := rows.Scan(&title, &privacy)
				if scanerr != nil {
					fmt.Fprintf(w, fmt.Sprint(scanerr))
					log.Println("Scan error: " + fmt.Sprint(scanerr))
					return
				}
				// Add the row to result.entries
				results.Entries = append(results.Entries, Result{Title: title, Privacy: privacy})
			}
			// Load the results.tmpl
			t, _ := template.ParseFiles("front_end_search_results.html")
			// Execute template with results
			t.Execute(w, results)
		}
	}
}

func login(w http.ResponseWriter, r *http.Request) {
	loginHTML, _ := template.ParseFiles("login_front_end.html")
	failHTML, _ := template.ParseFiles("front_end_flogin.html")
	if r.Method == "GET" {
		loginHTML.Execute(w, nil)
		// TODO: check whether a user is already logged in
	} else {
		r.ParseForm()
		
		Database, err := sql.Open("sqlite3", "keepmesafe.db")
		if err != nil {
			log.Fatal(err)
		}
		defer Database.Close()
		
		query := "SELECT username FROM User WHERE username = '" + r.Form["username"][0] + "' AND password = '" +
			r.Form["password"][0] + "'"

		var user string
		err = Database.QueryRow(query).Scan(&user)
		
		switch {
		case err != nil:
			// Failed login
			failHTML.Execute(w, nil)
			log.Printf("Failed login detected.")
		default:
			// Create and set cookies
			expiration := time.Now().Add(time.Hour)
			usernamecookie := http.Cookie{Name: "username", Value: user, Expires: expiration}
			http.SetCookie(w, &usernamecookie)
			session_id := fmt.Sprint(md5.Sum([]byte(user))) // no, this is not secure
			sessioncookie := http.Cookie{Name: "session_id", Value: session_id, Expires: expiration}
			http.SetCookie(w, &sessioncookie)
			// Add session_id to Session table
			// Delete first to avoid duplicates
			Database.Exec("DELETE FROM Session WHERE username = ?", user)
			Database.Exec("INSERT INTO Session VALUES (?, ?)", session_id, user)
			// Redirect to /search
			http.Redirect(w, r, "/search", http.StatusFound)
		}
		log.Println(query)
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	Database, err := sql.Open("sqlite3", "keepmesafe.db")
	if err != nil {
		log.Fatal(err)
	}
	defer Database.Close()

	// Remove session_id from Session table
	sessCookie, err := r.Cookie("session_id")
	if err != nil {
		// Redirect to /login
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
	log.Println("Removing session_id: " + sessCookie.Value)
	Database.Exec("DELETE FROM Session WHERE session_id = ?", sessCookie.Value)
	// Clear cookies
	http.SetCookie(w, &http.Cookie{Name: "username", Value: "", MaxAge: -1})
	http.SetCookie(w, &http.Cookie{Name: "session_id", Value: "", MaxAge: -1})

	// Redirect to /login
	http.Redirect(w, r, "/login", http.StatusFound)
}
