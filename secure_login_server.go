package main

import (
	"net/http"
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
)

func main() {
	http.HandleFunc("/login", SQLsecure)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func SQLsecure(w http.ResponseWriter, r *http.Request) {
	loginHTML := `
<html>
<head>
<title>Login</title>
</head>
<body>
<form action="/login" method="post">
	Username:<input type="text" name="username">
	Password:<input type="password" name="password">
	<input type="submit" value="Login">
</body>
</html>`

	if r.Method == "GET" {
		fmt.Fprintf(w, loginHTML)
	} else {
		r.ParseForm()
		username := r.Form["username"][0]
		log.Println("username", username)
		password := r.Form["password"][0]
		log.Println("password", password)
		
		Database, err := sql.Open("sqlite3", "keepmesafe.db")
		defer Database.Close()
		
		query := "SELECT username FROM User WHERE username = ? AND password = ?"
		
		var user string
		err = Database.QueryRow(query, username, password).Scan(&user)
		
		switch {
		case err == sql.ErrNoRows:
			fmt.Fprintf(w, loginHTML+"\nIncorrect username or password")
			log.Printf("Failed login detected.")
		case err != nil:
			fmt.Fprintf(w, fmt.Sprint(err))
			log.Println(err)
			log.Printf("SQL injection!")
		default:
			fmt.Fprintf(w, "Welcome, "+user)
			log.Printf("Successful login.")
		}
	}
}
